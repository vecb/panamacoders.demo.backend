# Primeros pasos con AngularJS y MVC - Agosto #

Esquema de la presentación para el Sab. 15/Agosto @6.00PM.    
Los demos serán walkthru desde scratch, la versión completa estará previamente subida al repositorio y además se hospedará una versión en producción online. Se tendrán algunos snippets pre hechos para acortar el tiempo.

### MVC/WebAPI ###
Tiempo: 45 min.

* Introducción - 5
* De MVC a WebAPI 15
    * Comparación - 3
    * Ejemplo de controller - 5
    * JSON y IHttpActionResult - 2
    * Estructura del proyecto - 5
* Demo - Simple Backend REST API - 17
    * Creación de un proyecto 2
    * Creación de un model y migrations 3
    * Scaffolding de un controller 5
    * Where is the view? -> Creación de un DTO 5
    * Llamada desde Postman 2
* CORS - 3 
* Going forward 5
    * Autenticación
    * Katana/ASP.NET
    * Dependency Injection

### Angular ###
Tiempo: 45 min.

* Introducción 2
* Partes de angular 5
* Dependency Injection 3
* Estructura del proyecto 5
* Demo - Simple Frontend SPA - 25
    * index.html como un IoC container 2
    * app.js y routing 5
    * Creación de un controller simple 5 
    * Creación de un directive simple 5 
    * Creación de un service (factory) para llamar al REST API 5
    * Ejecución 3
* Going forward 5
    * TypeScript
    * Angular2
    * Tools (Gulp, Browserify/Watchify)